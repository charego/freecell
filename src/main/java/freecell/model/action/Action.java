package freecell.model.action;

public interface Action {
    void redo();

    void undo();
}
