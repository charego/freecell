package freecell.util;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

public class ImageReader {

    public static Image readImage(String resourceName) {
        URL resource = ImageReader.class.getResource(resourceName);
        try {
            return ImageIO.read(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
