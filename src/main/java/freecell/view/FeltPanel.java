package freecell.view;

import static freecell.GraphicalApplication.DIMENSION;
import freecell.util.ImageReader;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;

public class FeltPanel extends JPanel {

    private final Image image;

    public FeltPanel() {
        image = ImageReader.readImage("/deck/FELT.jpg");
        super.setPreferredSize(DIMENSION);
        super.setMinimumSize(DIMENSION);
        super.setMaximumSize(DIMENSION);
        super.setSize(DIMENSION);
        super.setLayout(new BorderLayout());
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }
}
