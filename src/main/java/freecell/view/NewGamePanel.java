package freecell.view;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.FlowLayout;

public class NewGamePanel extends JPanel {

    private final JButton button = new JButton("New Game");

    public NewGamePanel() {
        setLayout(new FlowLayout());
        add(button);
        setOpaque(false);
    }

    public JButton getButton() {
        return button;
    }

}