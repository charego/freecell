package freecell.view.pile;

import freecell.model.pile.Pile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class StackedPilePanel extends PilePanel {

    public StackedPilePanel(Pile c) {
        super(c);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int x = (getWidth() - cardWidth) / 2;
        int y = (getHeight() - cardHeight) / 2;
        if (pile.isEmpty()) {
            // draw an outline
            g.setColor(Color.yellow);
            g.drawRect(x, y, cardWidth, cardHeight);
        } else {
            // draw the top card
            Image image = pile.topCard().image();
            g.drawImage(image, x, y, null);
        }
        if (isSelected()) {
            g.setColor(highlight);
            g.fillRect(x, y, cardWidth, cardHeight);
        }
    }
}
