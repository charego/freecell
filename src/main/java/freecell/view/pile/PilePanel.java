package freecell.view.pile;

import freecell.model.Card;
import freecell.model.pile.Pile;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Image;

public abstract class PilePanel extends JPanel {

    protected final Pile pile;
    private boolean isSelected = false;
    protected Color highlight = new Color(255, 255, 0, 150);

    protected static int cardWidth;
    protected static int cardHeight;

    static {
        Image cardBack = Card.cardBack();
        cardWidth = cardBack.getWidth(null);
        cardHeight = cardBack.getHeight(null);
    }

    public PilePanel(Pile pile) {
        this.pile = pile;
        super.setOpaque(false);
    }

    public Pile getPile() {
        return pile;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void toggleSelected() {
        isSelected = !isSelected;
        repaint();
    }

}
