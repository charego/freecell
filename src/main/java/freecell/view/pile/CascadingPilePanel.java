package freecell.view.pile;

import freecell.model.Card;
import freecell.model.pile.Tableau;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class CascadingPilePanel extends PilePanel {

    private static final int CARD_MARGIN = 22;
    private static final int TOP_MARGIN = 0;

    public CascadingPilePanel(Tableau t) {
        super(t);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int x = (getWidth() - cardWidth) / 2;
        int y = TOP_MARGIN;
        if (pile.isEmpty()) {
            // draw an outline
            g.setColor(Color.yellow);
            g.drawRect(x, y, cardWidth, cardHeight);
        } else {
            // draw the cards in a cascade from top to bottom
            for (Card card : pile) {
                Image image = card.image();
                g.drawImage(image, x, y, null);
                y += CARD_MARGIN;
            }
        }
        if (isSelected()) {
            if (pile.isEmpty()) {
                // highlight empty cell
                g.setColor(highlight);
                g.fillRect(x, y, cardWidth, cardHeight);
            } else {
                // highlight ordered cards
                if (y > TOP_MARGIN) {
                    y -= CARD_MARGIN;
                }
                int multiplier = ((Tableau) pile).topInOrder();
                int heightOfRect = (multiplier - 1) * CARD_MARGIN;
                g.setColor(highlight);
                g.fillRect(x, y - heightOfRect, cardWidth,
                        heightOfRect + cardHeight);
            }
        }
    }
}
