package freecell.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;

public class LabelPanel extends JPanel {

    public LabelPanel() {
        super.setLayout(new GridLayout(1, 2));
        addLabel("Free Cells");
        addLabel("Foundation Piles");
    }

    private void addLabel(String labelName) {
        JLabel label = new JLabel(labelName);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        super.add(label);
    }
}
