package freecell.view;

import freecell.model.Game;
import freecell.model.pile.Cell;
import freecell.model.pile.Foundation;
import freecell.model.pile.Tableau;
import freecell.view.pile.CascadingPilePanel;
import freecell.view.pile.PilePanel;
import freecell.view.pile.StackedPilePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class GamePanel extends JPanel {

    private final List<PilePanel> pilePanels = new ArrayList<>(16);

    public GamePanel(Game game) {
        for (Cell cell : game.getCells()) {
            pilePanels.add(new StackedPilePanel(cell));
        }
        for (Foundation foundation : game.getFoundations()) {
            pilePanels.add(new StackedPilePanel(foundation));
        }
        for (Tableau tableau : game.getTableaux()) {
            pilePanels.add(new CascadingPilePanel(tableau));
        }
        super.setLayout(new GridBagLayout());
        super.setOpaque(false);
        addConstraints();
    }

    private void addConstraints() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        super.add(pilePanels.get(0), gbc);
        gbc.gridx = GridBagConstraints.RELATIVE;
        for (int i = 1; i < 8; i++) {
            super.add(pilePanels.get(i), gbc);
        }
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weighty = 4;
        super.add(pilePanels.get(8), gbc);
        gbc.gridx = GridBagConstraints.RELATIVE;
        for (int i = 9; i < 16; i++) {
            super.add(pilePanels.get(i), gbc);
        }
    }

    public PilePanel determinePanelPressed(MouseEvent e) {
        if (e.getY() <= getHeight() / 5) {
            return pilePanels.get(e.getX() / (getWidth() / 8));
        } else {
            return pilePanels.get(e.getX() / (getWidth() / 8) + 8);
        }
    }

    public List<PilePanel> getFoundationPanels() {
        List<PilePanel> foundationPanels = new ArrayList<>(4);
        for (int i = 4; i < 8; i++) {
            foundationPanels.add(pilePanels.get(i));
        }
        return foundationPanels;
    }
}
